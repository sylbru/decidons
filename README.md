# Décidons

Outil de prise de décision utilisant le jugement majoritaire.

Prévu pour des situations « locales » telles qu’un choix de film, de jeu de société ou de lieu de sortie.

## Développement

```
npx vite
```

### Générer les classes Tailwind

```
npm run gen-tailwind
```

La génération utilise le fichier de configuration `tailwind.config.js`.