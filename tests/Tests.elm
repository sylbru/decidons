module Tests exposing (..)

import AssocList as Dict
import Candidate exposing (Candidate)
import Election exposing (Election)
import Expect
import Grade exposing (Grade(..))
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "Election"
        [ test "countVotes" <|
            \_ ->
                let
                    count =
                        Election.countVotes votesWithoutTies

                    expected =
                        Dict.fromList
                            (List.map2 Tuple.pair
                                candidates
                                [ [ Excellent, Acceptable, Poor, Reject, Reject ]
                                , [ Acceptable, Acceptable, Good, Poor, Reject ]
                                , [ Good, Excellent, Acceptable, Reject, Reject ]
                                , [ Excellent, Good, Good, Good, Excellent ]
                                ]
                            )
                            |> Dict.map (\_ grades -> List.sortBy Grade.toComparable grades)
                in
                if Dict.eq count expected then
                    Expect.pass

                else
                    Expect.fail "Not equal"

        --, findMajorityGrades
        --, findMajorityGrade
        , findWinner
        ]



--findMajorityGrades : Test
--findMajorityGrades =
--    describe "findMajorityGrades"
--        [ test "Odd number of voters" <|
--            \_ ->
--                (Dict.fromList
--                    [ ( "The Crew", [ Excellent, Acceptable, Poor, Reject, Reject ] )
--                    , ( "Compagnons", [ Acceptable, Acceptable, Good, Poor, Reject ] )
--                    , ( "Imagine", [ Good, Excellent, Acceptable, Reject, Reject ] )
--                    , ( "Ligretto", [ Excellent, Good, Good, Good, Excellent ] )
--                    ]
--                    |> Dict.map (\_ grades -> List.sort grades)
--                )
--                    |> Election.findMajorityGrades
--                    |> Expect.equal
--                        (Dict.fromList
--                            [ ( "The Crew", Poor )
--                            , ( "Compagnons", Acceptable )
--                            , ( "Imagine", Acceptable )
--                            , ( "Ligretto", Good )
--                            ]
--                        )
--        , test "Even number of voters" <|
--            \_ ->
--                (Dict.fromList
--                    [ ( "The Crew", [ Excellent, Acceptable, Reject, Reject ] )
--                    , ( "Compagnons", [ Acceptable, Acceptable, Poor, Reject ] )
--                    , ( "Imagine", [ Good, Excellent, Reject, Reject ] )
--                    , ( "Ligretto", [ Excellent, Good, Good, Excellent ] )
--                    ]
--                    |> Dict.map (\_ grades -> List.sort grades)
--                )
--                    |> Election.findMajorityGrades
--                    |> Expect.equal
--                        (Dict.fromList
--                            [ ( "The Crew", Reject )
--                            , ( "Compagnons", Poor )
--                            , ( "Imagine", Reject )
--                            , ( "Ligretto", Good )
--                            ]
--                        )
--        ]
--findMajorityGrade : Test
--findMajorityGrade =
--    describe "findMajorityGrade"
--        [ test "Odd number of voters" <|
--            \_ ->
--                Election.findMajorityGrade [ Good, Good, Good, Excellent, Excellent ]
--                    |> Expect.equal Good
--        , test "Odd number of voters (bis)" <|
--            \_ ->
--                Election.findMajorityGrade [ Reject, Poor, Acceptable, Good, Excellent ]
--                    |> Expect.equal Acceptable
--        , test "Even number of voters" <|
--            \_ ->
--                Election.findMajorityGrade [ Good, Good, Excellent, Excellent ]
--                    |> Expect.equal Good
--        , test "Even number of voters (bis)" <|
--            \_ ->
--                Election.findMajorityGrade [ Reject, Poor, Acceptable, Good ]
--                    |> Expect.equal Poor
--        , test "Odd number of voters with votes remaining until next grade" <|
--            \_ ->
--                Election.findMajorityGrade [ Reject, Reject, Reject, Reject, Poor ]
--                    |> Expect.equal Reject
--        , test "Even number of voters with votes remaining until next grade" <|
--            \_ ->
--                Election.findMajorityGrade [ Reject, Poor, Acceptable, Good ]
--                    |> Expect.equal Poor
--        ]


findWinner : Test
findWinner =
    describe "findWinner"
        [ test "no tie" <|
            \_ ->
                Election.findWinner votesWithoutTies
                    |> List.map Candidate.toString
                    |> Expect.equal [ "Ligretto" ]
        , test "with a tie" <|
            \_ ->
                Election.findWinner votesWithTie
                    |> List.map Candidate.toString
                    |> Expect.equal [ "Imagine" ]
        ]


candidates : List Candidate
candidates =
    [ "The Crew", "Compagnons", "Imagine", "Ligretto" ]
        |> List.map Candidate.fromString
        |> List.filterMap identity


votesWithoutTies : Election
votesWithoutTies =
    Election.new
        |> Election.addVote (Dict.fromList <| List.map2 Tuple.pair candidates [ Excellent, Acceptable, Good, Excellent ])
        |> Election.addVote
            (Dict.fromList <| List.map2 Tuple.pair candidates [ Acceptable, Acceptable, Excellent, Good ])
        |> Election.addVote
            (Dict.fromList <| List.map2 Tuple.pair candidates [ Poor, Good, Acceptable, Good ])
        |> Election.addVote
            (Dict.fromList <| List.map2 Tuple.pair candidates [ Reject, Poor, Reject, Good ])
        |> Election.addVote
            (Dict.fromList <| List.map2 Tuple.pair candidates [ Reject, Reject, Reject, Excellent ])


votesWithTie : Election
votesWithTie =
    Election.new
        |> Election.addVote
            (Dict.fromList <| List.map2 Tuple.pair candidates [ Excellent, Acceptable, Good, Excellent ])
        |> Election.addVote
            (Dict.fromList <| List.map2 Tuple.pair candidates [ Acceptable, Acceptable, Excellent, Reject ])
        |> Election.addVote
            (Dict.fromList <| List.map2 Tuple.pair candidates [ Poor, Good, Acceptable, Good ])
        |> Election.addVote
            (Dict.fromList <| List.map2 Tuple.pair candidates [ Reject, Poor, Excellent, Acceptable ])
        |> Election.addVote
            (Dict.fromList <| List.map2 Tuple.pair candidates [ Reject, Reject, Good, Excellent ])
