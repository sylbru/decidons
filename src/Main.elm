module Main exposing (..)

import AssocList as Dict exposing (Dict)
import Browser
import Candidate exposing (Candidate)
import Css
import Css.Global
import Css.Transitions
import Election exposing (Election, Vote)
import EverySet exposing (EverySet)
import Grade exposing (Grade)
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attr exposing (css)
import Html.Styled.Events as Events
import Json.Decode as Decode exposing (Decoder)
import Tailwind.Breakpoints as Breakpoints
import Tailwind.Utilities as Tw


type Model
    = Home
    | Setup String (EverySet Candidate)
    | Voting VotingModel
    | Results Election


type alias VotingModel =
    { candidates : EverySet Candidate
    , votes : Election
    , currentVote : Maybe Vote
    }


type Msg
    = ClickedSetupVote
    | SubmittedNewCandidate
    | TypedCurrentCandidate String
    | ClickedStartVoting
    | ClickedVote
    | SubmittedVote
    | SetCurrentGradeForCandidate Candidate Grade String
    | ClickedCloseVote
    | ClickedHome


init : () -> ( Model, Cmd Msg )
init _ =
    ( Home
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ClickedSetupVote ->
            case model of
                Home ->
                    ( Setup "" EverySet.empty, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        SubmittedNewCandidate ->
            case model of
                Setup current candidates ->
                    case Candidate.fromString current of
                        Just newCandidate ->
                            ( Setup "" (EverySet.insert newCandidate candidates), Cmd.none )

                        Nothing ->
                            ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        TypedCurrentCandidate newValue ->
            case model of
                Setup _ candidates ->
                    ( Setup newValue candidates, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        ClickedStartVoting ->
            case model of
                Setup _ candidates ->
                    ( Voting { candidates = candidates, votes = Election.new, currentVote = Nothing }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        ClickedVote ->
            case model of
                Voting votingModel ->
                    if votingModel.currentVote == Nothing then
                        ( Voting { votingModel | currentVote = Just Dict.empty }, Cmd.none )

                    else
                        ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        SubmittedVote ->
            case model of
                Voting votingModel ->
                    ( submitVote votingModel model
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )

        SetCurrentGradeForCandidate candidate grade _ ->
            case ( model, grade ) of
                ( Voting votingModel, grade_ ) ->
                    ( Voting
                        { votingModel
                            | currentVote =
                                votingModel.currentVote
                                    |> Maybe.map (Dict.insert candidate grade_)
                        }
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )

        ClickedCloseVote ->
            case model of
                Voting { votes } ->
                    ( Results votes, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        ClickedHome ->
            case model of
                Results _ ->
                    ( Home, Cmd.none )

                _ ->
                    ( model, Cmd.none )


submitVote : VotingModel -> Model -> Model
submitVote votingModel model =
    case votingModel.currentVote of
        Just currentVote_ ->
            if
                setEquals
                    (EverySet.fromList (Dict.keys currentVote_))
                    votingModel.candidates
            then
                Voting
                    { votingModel
                        | votes = Election.addVote currentVote_ votingModel.votes
                        , currentVote = Nothing
                    }

            else
                model

        Nothing ->
            model


setEquals : EverySet a -> EverySet a -> Bool
setEquals setA setB =
    (EverySet.size setA == EverySet.size setB)
        && (EverySet.size (EverySet.intersect setA setB) == EverySet.size setB)


view : Model -> Html Msg
view model =
    let
        content =
            case model of
                Home ->
                    Html.div [ css [ Tw.flex, Tw.justify_center, Tw.items_center, Tw.h_full ] ]
                        [ Html.button [ css [ buttonStyle ], Events.onClick ClickedSetupVote ] [ Html.text "Organiser un vote" ]
                        ]

                Setup current candidates ->
                    Html.div [ css [ Tw.flex, Tw.flex_col, Tw.justify_between, Tw.h_full ] ]
                        [ Html.div [ css [ Tw.flex_1, Tw.flex, Tw.items_center ] ]
                            [ Html.ul [ css [ ulCandidatesStyle ] ]
                                (List.map viewCandidateSetup (List.reverse <| EverySet.toList candidates))
                            ]
                        , Html.div []
                            [ Html.form
                                [ Events.onSubmit SubmittedNewCandidate
                                , css [ Tw.flex, Tw.gap_2, Css.marginBottom (Css.vh 5) ]
                                ]
                                [ Html.input
                                    [ Attr.value current
                                    , Events.onInput TypedCurrentCandidate
                                    , css [ Tw.flex_1, inputStyle ]
                                    ]
                                    []
                                , Html.button [ Attr.type_ "submit", css [ buttonStyle ] ] [ Html.text "Ajouter" ]
                                ]
                            , Html.button [ css [ buttonStyle, Tw.w_full ], Events.onClick ClickedStartVoting ] [ Html.text "Passer au vote" ]
                            ]
                        ]

                Voting { candidates, votes, currentVote } ->
                    case currentVote of
                        Nothing ->
                            Html.div [ css [ Tw.flex, Tw.flex_col, Tw.justify_between, Tw.h_full, Tw.gap_4 ] ]
                                [ Html.ul [ css [ ulCandidatesStyle ] ]
                                    (List.map viewCandidateSetup (List.reverse <| EverySet.toList candidates))
                                , Html.p [ css [ Tw.text_center ] ] [ Html.text <| votersCountToString votes ]
                                , Html.div [ css [ Tw.flex, Tw.gap_2 ] ]
                                    [ Html.button [ css [ buttonStyle, Tw.flex_1 ], Events.onClick ClickedVote ] [ Html.text "Voter" ]
                                    , Html.button [ css [ buttonStyle, Tw.flex_1 ], Events.onClick ClickedCloseVote ] [ Html.text "Clôre le vote" ]
                                    ]
                                ]

                        Just currentVote_ ->
                            viewVoteForm candidates currentVote_

                Results votes ->
                    Html.div [ css [ Tw.flex, Tw.flex_col, Tw.justify_between, Tw.h_full, Tw.gap_4 ] ]
                        [ viewElectionWinner votes
                        , viewElectionMeritProfiles votes
                        , Html.button [ css [ buttonStyle ], Events.onClick ClickedHome ] [ Html.text "Retour à l’accueil" ]
                        ]
    in
    Html.div
        [ css
            [ Tw.bg_fuchsia_100
            , Tw.text_fuchsia_1000
            , Tw.p_5
            , Tw.h_full
            , Tw.box_border
            , Tw.font_sans
            , Tw.max_w_lg
            , Tw.mx_auto
            ]
        ]
        [ content
        , Css.Global.global
            (Tw.globalStyles
                ++ [ Css.Global.each [ Css.Global.input, Css.Global.button ]
                        [ Css.property "accent-color" "rgb(223, 72, 247)"
                        ]
                   ]
            )
        ]


viewCandidateSetup : Candidate -> Html Msg
viewCandidateSetup candidate =
    Html.li
        [ css
            [ Tw.list_none
            , Tw.text_center
            , Tw.text_lg
            , Tw.py_2
            , Tw.px_4
            , Tw.bg_white
            , Tw.rounded
            , Tw.shadow_md
            , Tw.inline_block
            , Tw.m_4
            ]
        ]
        [ Html.text (Candidate.toString candidate) ]


onEnter : msg -> Html.Attribute msg
onEnter msg =
    let
        isEnter : String -> Decoder msg
        isEnter key =
            if key == "Enter" then
                Decode.succeed msg

            else
                Decode.fail "not Enter"

        decoder =
            Decode.field "code" Decode.string
                |> Decode.andThen isEnter
    in
    Events.on "keyup" decoder


buttonStyle : Css.Style
buttonStyle =
    Css.batch
        [ Tw.bg_fuchsia_700
        , Tw.text_white
        , Tw.px_4
        , Tw.text_base
        , Tw.py_3
        , Tw.border_0
        , Tw.rounded_tl_md
        , Tw.rounded_br_md
        , Tw.shadow_lg
        , Tw.whitespace_nowrap
        , Tw.cursor_pointer
        , Css.Transitions.transition [ Css.Transitions.backgroundColor 200 ]
        , Css.hover [ Tw.bg_fuchsia_600 ]
        ]


inputStyle : Css.Style
inputStyle =
    Css.batch
        [ Tw.text_base
        , Tw.min_w_0
        , Tw.shadow_md
        , Tw.rounded_tl_md
        , Tw.rounded_br_md
        , Tw.px_2
        , Tw.border_fuchsia_700
        , Css.Transitions.transition [ Css.Transitions.borderColor 200 ]
        , Css.hover [ Tw.border_fuchsia_500 ]
        , Tw.border_2
        ]


ulCandidatesStyle : Css.Style
ulCandidatesStyle =
    Css.batch
        [ Tw.p_0
        , Tw.m_0
        , Tw.overflow_y_scroll
        , Tw.mx_auto
        , Tw.text_center
        ]


votersCountToString : Election -> String
votersCountToString election =
    case Election.votesCount election of
        0 ->
            "Aucun vote enregistré"

        1 ->
            "1 vote enregistré"

        n ->
            String.fromInt n ++ " votes enregistrés"


viewVoteForm : EverySet Candidate -> Vote -> Html Msg
viewVoteForm candidates currentVote =
    let
        viewCandidate : Candidate -> List (Html Msg)
        viewCandidate candidate =
            let
                currentVoteGrade =
                    Dict.get candidate currentVote

                gradeOption : Grade -> Html Msg
                gradeOption grade =
                    Html.label
                        [ css
                            [ Tw.flex
                            , Tw.flex_1
                            , Tw.flex_col_reverse
                            , Tw.items_center
                            , Tw.p_2
                            , Tw.whitespace_nowrap
                            , Tw.cursor_pointer
                            , Tw.bg_fuchsia_50
                            , Tw.rounded
                            , Css.hover [ Tw.bg_white ]
                            , Css.Transitions.transition [ Css.Transitions.backgroundColor 200 ]
                            ]
                        ]
                        [ Html.input
                            [ Attr.type_ "radio"
                            , Attr.checked (currentVoteGrade == Just grade)
                            , Events.onInput (SetCurrentGradeForCandidate candidate grade)
                            , Attr.name ("vote_" ++ Candidate.toString candidate)
                            , Attr.value (Grade.toValue grade)
                            ]
                            []
                        , Html.span [ Attr.style "user-select" "none" ] [ Html.text (Grade.toString grade) ]
                        ]
            in
            [ Html.fieldset [ css [ Tw.my_8, Css.firstChild [ Tw.my_0 ] ] ]
                [ Html.legend [ css [ Tw.font_bold, Tw.mx_auto, Tw.mb_2 ] ] [ Html.text (Candidate.toString candidate) ]
                , Html.div [ css [ Tw.flex ] ]
                    (List.map gradeOption Grade.all)
                ]
            ]
    in
    Html.form
        [ Events.onSubmit SubmittedVote
        , css [ Tw.flex, Tw.flex_col, Tw.gap_4, Tw.h_full ]
        ]
    <|
        [ Html.div [ css [ Tw.flex_1, Tw.overflow_y_scroll ] ]
            (List.concatMap viewCandidate <| List.reverse <| EverySet.toList candidates)
        , Html.button [ css [ buttonStyle ], Attr.type_ "submit" ] [ Html.text "Voter" ]
        ]


viewElectionWinner : Election -> Html Msg
viewElectionWinner election =
    let
        winnerCandidate : List Candidate
        winnerCandidate =
            Election.findWinner election

        viewWinner : Candidate -> Html Msg
        viewWinner candidate =
            Html.strong
                [ css [ Tw.text_xl ] ]
                [ Html.text (Candidate.toString candidate)
                ]
    in
    Html.div
        []
        [ Html.p [ css [ Tw.text_center, Tw.p_6 ] ]
            (case winnerCandidate of
                [ singleWinnerCandidate ] ->
                    [ Html.text "Option préférée\u{00A0}: "
                    , Html.br [] []
                    , viewWinner singleWinnerCandidate
                    ]

                [] ->
                    [ Html.text "Pas assez de votes." ]

                severalTiedWinners ->
                    [ Html.text "Options préférées\u{00A0}: "
                    , Html.br [] []
                    , Html.div []
                        (List.map viewWinner severalTiedWinners
                            |> List.intersperse (Html.text ", ")
                        )
                    ]
            )
        ]


viewElectionMeritProfiles : Election -> Html Msg
viewElectionMeritProfiles election =
    Html.div
        [ css [ Tw.overflow_y_scroll ] ]
        (List.map
            (\( candidate, grades ) -> viewCandidateMeritProfile candidate grades)
            (Dict.toList <| Election.countVotes election)
        )


viewCandidateMeritProfile : Candidate -> List Grade -> Html Msg
viewCandidateMeritProfile candidate grades =
    Html.div [ css [ Tw.py_2 ] ]
        [ Html.text (Candidate.toString candidate)
        , Html.div
            [ css [ Css.displayFlex ] ]
            (List.map (viewGradeInMeritProfile grades) Grade.all)
        ]


viewGradeInMeritProfile : List Grade -> Grade -> Html Msg
viewGradeInMeritProfile grades grade =
    let
        percentage : Int
        percentage =
            toPercent grade grades
                |> round

        label : String
        label =
            Grade.toString grade ++ " : " ++ String.fromInt percentage ++ " %"
    in
    Html.div
        [ css
            [ Css.flexBasis (Css.pct <| toPercent grade grades)
            , Css.display Css.inlineBlock
            , Css.backgroundColor (Grade.toColor grade)
            , Css.height (Css.px 20)
            ]
        ]
        [ Html.span [ Attr.title label, Attr.attribute "aria-label" label ] [] ]


toPercent : Grade -> List Grade -> Float
toPercent grade grades =
    let
        givenGradeCount =
            List.filter ((==) grade) grades
                |> List.length
                |> toFloat

        allGradesCount =
            List.length grades
                |> toFloat
    in
    (givenGradeCount / allGradesCount)
        |> (*) 100


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view = view >> Html.toUnstyled
        , subscriptions = \_ -> Sub.none
        }
