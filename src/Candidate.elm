module Candidate exposing (Candidate, fromString, toString)


type Candidate
    = Candidate String


fromString : String -> Maybe Candidate
fromString candidateString =
    if not <| String.isEmpty candidateString then
        Just <| Candidate candidateString

    else
        Nothing


toString : Candidate -> String
toString (Candidate candidateString) =
    candidateString
