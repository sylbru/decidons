module Election exposing (Election, Vote, addVote, countVotes, findWinner, new, votesCount)

import AssocList as Dict exposing (Dict)
import Candidate exposing (Candidate)
import Grade exposing (Grade)
import List.Extra


type Election
    = Election (List Vote)


type alias Vote =
    Dict Candidate Grade


type alias Count =
    Dict Candidate (List Grade)


new : Election
new =
    Election []


addVote : Vote -> Election -> Election
addVote vote (Election votes) =
    Election (vote :: votes)


findWinner : Election -> List Candidate
findWinner election =
    election
        |> countVotes
        |> findWinnerFromCount


findWinnerFromCount : Count -> List Candidate
findWinnerFromCount count =
    let
        potentialWinners : List ( Candidate, Grade )
        potentialWinners =
            count
                |> findMajorityGrades
                |> Dict.toList
                |> List.foldl
                    (\( currentCandidate, currentMajorityGrade ) bestOneSoFar ->
                        case bestOneSoFar of
                            [] ->
                                [ ( currentCandidate, currentMajorityGrade ) ]

                            ( _, bestMajorityGradeSoFar ) :: _ ->
                                case
                                    compare
                                        (currentMajorityGrade |> Grade.toComparable)
                                        (bestMajorityGradeSoFar |> Grade.toComparable)
                                of
                                    GT ->
                                        [ ( currentCandidate, currentMajorityGrade ) ]

                                    EQ ->
                                        ( currentCandidate, currentMajorityGrade ) :: bestOneSoFar

                                    LT ->
                                        bestOneSoFar
                    )
                    []
    in
    if List.any (\( _, grades ) -> List.isEmpty grades) (Dict.toList count) then
        Dict.toList count |> List.map Tuple.first

    else
        case potentialWinners of
            [ singleWinner ] ->
                [ Tuple.first singleWinner ]

            [] ->
                []

            tiedCandidates ->
                count
                    |> Dict.filter (\candidate _ -> List.member candidate (List.map Tuple.first tiedCandidates))
                    |> Dict.map (\_ grades -> dropMiddleVoter grades)
                    |> findWinnerFromCount


findMajorityGrades : Count -> Dict Candidate Grade
findMajorityGrades =
    Dict.map (\_ grades -> findMajorityGrade grades)


{-| From a list of grades, finds the majority grade
-}
findMajorityGrade : List Grade -> Grade
findMajorityGrade grades =
    grades
        |> List.sortBy Grade.toValue
        |> List.Extra.getAt (middleVoter grades)
        |> Maybe.withDefault Grade.default


middleVoter : List Grade -> Int
middleVoter grades =
    let
        votersCount : Int
        votersCount =
            List.length grades
    in
    (if isEven votersCount then
        List.length grades // 2

     else
        (List.length grades // 2) + 1
    )
        -- Indices are represented before being decremented (to account for lists being 0-indexed),
        -- this way the expression is less confusing when compared with how we would express it in
        -- mathematical terms:
        -- "If the number of the voters is odd of the form 2N + 1, we take the Nth voter"
        - 1


dropMiddleVoter : List Grade -> List Grade
dropMiddleVoter grades =
    List.Extra.removeAt (middleVoter grades) grades


countVotes : Election -> Count
countVotes (Election votes) =
    let
        countSingleVote : Vote -> Count -> Count
        countSingleVote vote count =
            let
                countGrade : ( Candidate, Grade ) -> Count -> Count
                countGrade ( candidate, grade ) count_ =
                    Dict.update
                        candidate
                        (\maybeGrades ->
                            Just <|
                                case maybeGrades of
                                    Just grades ->
                                        grade :: grades

                                    Nothing ->
                                        [ grade ]
                        )
                        count_
            in
            List.foldl countGrade count (Dict.toList vote)
    in
    List.foldl countSingleVote Dict.empty votes
        |> Dict.map (\_ grades -> List.sortBy Grade.toValue grades)


votesCount : Election -> Int
votesCount (Election votes) =
    List.length votes


isEven : Int -> Bool
isEven n =
    (n |> modBy 2) == 0
