module Grade exposing (Grade(..), all, default, toColor, toComparable, toString, toValue)

import Css exposing (Color)


type Grade
    = Excellent
    | Good
    | Acceptable
    | Poor
    | Reject


all : List Grade
all =
    [ Excellent, Good, Acceptable, Poor, Reject ]


toComparable : Grade -> Int
toComparable grade =
    case grade of
        Excellent ->
            5

        Good ->
            4

        Acceptable ->
            3

        Poor ->
            2

        Reject ->
            1


toValue : Grade -> String
toValue =
    toComparable >> String.fromInt


toString : Grade -> String
toString grade =
    case grade of
        Excellent ->
            "Excellent"

        Good ->
            "Bien"

        Acceptable ->
            "Assez bien"

        Poor ->
            "Insuffisant"

        Reject ->
            "À rejeter"


toColor : Grade -> Color
toColor grade =
    case grade of
        Reject ->
            Css.hex "#ff4500"

        Poor ->
            Css.hex "#ffa500"

        Acceptable ->
            Css.hex "#ffff00"

        Good ->
            Css.hex "#9acd32"

        Excellent ->
            Css.hex "#228b22"


default : Grade
default =
    Reject
