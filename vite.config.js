import elmPlugin from "vite-plugin-elm"

export default {
    plugins: [elmPlugin()],
    build: {
        outDir: "build",
        target: "es2020",
    }
}